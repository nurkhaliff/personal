# Alief's Personal Git
## Cloning repo
1. Open CMD in the target folder
2. Use git clone + http link from gitlab

## Commit vs Push
Commit means making changes to the local dir. Push means making those changes to the repo in gitlab.  
**Commit:**
1. Open CMD
2. Use git add -A ('A' stands for all)
2. Use git commit
4. Enter commit message, save, then exit  

**Push:**
1. Open CMD
2. Use git push origin <branch>